abstract class Baz {
    private int id;
    private String name;

    Baz(int id, String name) {
        this.id = id;
        this.name = name;
    }

    String getName() {
        return name;
    }
    Integer getID() {
        return id;
    }

    abstract double getAveragePayment();

    @Override
    public String toString() {
        return "№" + id + " " + name;
    }
}