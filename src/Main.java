import java.util.*;
public class Main {
    public static void main(String[] args) {
        int id = 0;
        Set<Baz> rab= new TreeSet<>(new Comparator<Baz>() {
            public int compare(Baz a, Baz b) {
                double diff = a.getAveragePayment() - b.getAveragePayment();
                if ( diff > 0.0 )
                    return -1;
                else if ( diff < 0.0 )
                    return 1;
                else {
                    int n = a.getName().compareTo(b.getName());
                    return ( n == 0 ) ? 1 : n;
                }
            }
        });

        rab.add(new FiksOplata(++id, "Василий", 3400));
        rab.add(new FiksOplata(++id, "Иван", 1300));
        rab.add(new ChasOplata(++id, "Екатерина", 60));
        rab.add(new FiksOplata(++id, "Валерия", 1500));
        rab.add(new FiksOplata(++id, "Кирилл", 700));
        rab.add(new FiksOplata(++id, "Анастасия", 2500));
        rab.add(new ChasOplata(++id, "Мария", 150));
        rab.add(new ChasOplata(++id, "Надежда", 30));
        rab.add(new FiksOplata(++id, "Евдакия", 1600));
        rab.add(new ChasOplata(++id, "Кристина", 100));
        Baz[] arr = new Baz[ rab.size() ];
        rab.toArray(arr);
        System.out.println("Упорядочить всю последовательность рабочих по убыванию среднемесячной зарплаты (Вывести Зарплату ,Идентификатор,Имя):");
        for ( int i = 0; i < arr.length; ++i )
            System.out.println(arr[i]);
        System.out.println("Вывести первые 5 имен работников из полученного выше списка:");
        for ( int i = 0; i < 5; ++i )
            System.out.println(arr[i]);
        System.out.println("Вывести последние 3 идентификатора работников из полученного выше списка:");
        for ( int i = arr.length-3; i < arr.length; ++i )
            System.out.println(arr[i].getID());
    }
}