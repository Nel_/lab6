import java.text.NumberFormat;
import java.util.Locale;
class FiksOplata extends Baz{
    private double monthPayment;

    FiksOplata(int id, String name, double monthPayment) {
        super(id, name);
        this.monthPayment = monthPayment;
    }

    @Override
    double getAveragePayment() {
        return monthPayment;
    }

    @Override
    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance(Locale.US);
        return super.toString() + " фиксированная оплата " + fmt.format(getAveragePayment());
    }
}